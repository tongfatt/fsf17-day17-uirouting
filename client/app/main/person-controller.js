(function () {
    angular
        .module("uirouterApp")
        .controller("PersonCtrl", ["$stateParams", "PeopleService", PersonCtrl]);

    function PersonCtrl( $stateParams, PeopleService){
        var vm = this;
        console.log("PersonCtrl controller --> ");
        var personId = $stateParams.personId;
        console.log(personId);
        PeopleService.getPerson(personId).then(function(result){
            vm.person = result;  
        });
    }

})();